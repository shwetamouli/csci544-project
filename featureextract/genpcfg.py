from sys import argv
import pickle

tweet_file=argv[1]
#print(tweet_file)

txt=open(tweet_file,'r',errors='ignore')
txtr=open('pcfgres.txt','wb')

wdict={}
terminal_dict={}
pcfg={}
list_tweet=[]
counter=0
for line in txt:
	line = line.rstrip()
	list1=line.split('	')
	#print(line)
	#print(listw)
	
	if len(list1)==1:
		#print('\n\n')
		
		
		n=len(list_tweet)
		for x in range(0,n):
			list_rhs=[]
			for item in list_tweet:
				#print(item[3])
				if item[3]==str(x):
					list_rhs.append(item[2])
			#print(list_rhs)
			if x==0 and len(list_rhs)!=0:
				sent='S -> '+(' '.join(list_rhs))
				#print(sent)
				if sent not in pcfg:
					pcfg[sent]=1
				else:
					pcfg[sent]=pcfg[sent]+1
			elif len(list_rhs)!=0:
				sent=list_tweet[x-1][2]+' -> '+(' '.join(list_rhs))
				#print(sent)
				if sent not in pcfg:
					pcfg[sent]=1
				else:
					pcfg[sent]=pcfg[sent]+1
					
		#print(list_tweet)
		counter=counter+1			
		list_tweet=[]
		continue
	else:
		listw=[]
		listw.append(list1[0])
		listw.append(list1[1])
		listw.append(list1[3])
		listw.append(list1[6])
		
		#print(listw)
		
		if listw[1] not in wdict:
			wdict[listw[1]]=1
		else:
			wdict[listw[1]]=wdict[listw[1]]+1
		
		if listw[1] not in terminal_dict:
			terminal_dict[listw[1]+','+listw[2]]=1
		else:
			terminal_dict[listw[1]+','+listw[2]]=terminal_dict[listw[1]+','+listw[2]]+1
		
		list_tweet.append(listw)
	
	
		

		
#print(wdict)	
val_dict={}
prob_dict={}
format_dict={}
terminals={}
tcount={}
letters_dict={}
for item in terminal_dict:
	val=item.split(',')
	if val[1] not in letters_dict and val[1]!='':
		letters_dict[val[1]]=1

for it in letters_dict:
	maxi=0
	for item in terminal_dict:
		val=item.split(',')
		if val[1]==it:
			if maxi<terminal_dict[item]:
				maxi=terminal_dict[item]
				terminals[it]=val[1]+' -> \''+val[0] +'\''
				tcount[it]=terminal_dict[item]
for item in pcfg:
	final_list=item.split(' ')
	#print(final_list[0])
	if final_list[0] not in val_dict:
		val_dict[final_list[0]] = pcfg[item]
	else:
		val_dict[final_list[0]] = val_dict[final_list[0]] + pcfg[item]
#print(val_dict)
#recaliberate
for item in val_dict:
	if item!='S':
		val_dict[item]=val_dict[item]+tcount[item]
#print(val_dict)

for item in pcfg:
	final_list=item.split(' ')
	if final_list[0] in val_dict:
		prob_dict[item]=((pcfg[item])/(val_dict[final_list[0]]))
		#print(prob_dict[item])

for item in prob_dict:
	
	#print(final_list)
	for it in val_dict:
		final_list=item.split(' ')
		if it == final_list[0]:
			if it not in format_dict:
				format_dict[it] = item + ' [' +str(prob_dict[item]) + ']'
				#format_dict[it] = item
			else:
				#final_list.pop(0)
				final_list=final_list[2:]				
				#print(final_list)
				#final_list=final_list
				#final_list.pop(0)
				#print(len(final_list))				
				tempstr=' '.join(final_list)
				format_dict[it] = format_dict[it]+ ' | '+ tempstr+ ' [' + str(prob_dict[item]) + ']'
				#format_dict[it] = format_dict[it]+ ' | '+ tempstr
#print(pcfg)
#print('\n\n')
#print(prob_dict)
#print('\n\n')
#print(val_dict)
#print('\n\n')
#print(format_dict)
#print('\n\n')
#print(len(pcfg))
#print(len(prob_dict))


#print('\n\n')
#print(counter)
#print(terminal_dict)

#print(letters_dict)
''' to print the pcfg
#print('\n\n')
#print(terminals)
#print('\n\n')
#for item in format_dict:
	print(format_dict[item])
#print('\n\n')

#gen num
#print(tcount)
#print(val_dict)
#print(terminals)
for item in terminals:
	if item in val_dict:
		print(terminals[item]+ ' ['+str(tcount[item]/val_dict[item])+']')
	else:
		print(terminals[item]+ ' [1.0]')
#for item in terminals:
	#print(terminals[item]+ ' [1.0]')
	#print(terminals[item])'''

pickle.dump(prob_dict,txtr)
txtr.close()
txt.close()
