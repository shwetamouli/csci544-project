import nltk
from nltk import CFG
from nltk import PCFG
from nltk import Tree
from random import choice
import os
import sys
# This funcion is based on _generate_all() in nltk.parse.generate
# It therefore assumes the same import environment otherwise.
def getmaxlist(d):
 vals=d.values()
 maxval=max(vals)
 l=[k for k in d if d[k]==maxval]
 return l 



def produce(grammar, symbol):
    words = []
    #print(symbol)
    productions = grammar.productions(lhs = symbol)
    #print(productions)
    d={k:k.prob() for k in productions}
    l=getmaxlist(d)		  	
    production = choice(l)
    #print(production)
    for sym in production.rhs():
 	#print('sym='+str(type(sym)))
        if isinstance(sym, basestring):
            words.append(sym)
            #print('words='+str(words))
        else:
            words.extend(produce(grammar, sym))
    return words

'''t=Tree.fromstring("""(ROOT
  (S
    (ADVP (RB Now))
    (, ,)
    (NP (PRP you))
    (VP (MD can)
      (VP (VB be)
        (VP (VBN entertained))))
    (. .)))""")'''
#t=Tree.fromstring("(S (NP I) ( VP (VP (V shot)(NP (Det an)(N elephant)))(PP (P in)(NP(Det my)(N pajamas)))))")
'''t=Tree.fromstring("""
(ROOT
  (NP
    (NP (JJR More) (NNS #StarWars))
    (: :)
    (NP
      (NP (NNP Battlefront) (NNP -LSB-) (NNP RP))
      (SBAR
        (S
          (NP (JJ -RSB-) (NNS details))
          (VP (VBD included)
            (PP (IN in)
              (NP (DT a) (NNP Q&A)))
            (PP (IN with)
              (NP
                (NP (DT the) (NNS games) (NN developer))
                (: :)
                (NP (JJ http://t.co/y49R8tt8U8) (NNS http://t.co/viGlVBGb88))))))))))""")'''
gfile=sys.argv[1]
f=open(gfile,'r')
s=str(f.read())
s=s.replace('-LSB-','[')
s=s.replace('-RSB-',']')
s=s.replace('-LCB-','{')
s=s.replace('-RCB-','}')
s=s.replace('-LRB-','(')
s=s.replace('-RRB-',')')

t=Tree.fromstring(s)

print(t)
allProductions = t.productions()
S = nltk.Nonterminal('S')
grammar = nltk.induce_pcfg(S, allProductions)
print(grammar)
#out=generate_sample(grammar)
#print(grammar.start())
sentlist=[]
#for i in range(10):
 #sentlist.append(' '.join(produce(grammar,grammar.start())))
 #print(sentlist[i]+' '+str(len(sentlist[i].split())))'''
'''print()
print()'''

while True:
 tweet=(' '.join(produce(grammar,grammar.start())))
 if(len(tweet)<=140):
  break
print tweet
#print(pcfg)
#productions = pcfg.productions(lhs = pcfg.start())
#print(productions)
#print(' '.join(produce(pcfg,pcfg.start())))







