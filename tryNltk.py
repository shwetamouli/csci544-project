__author__ = 'Shweta'
import nltk
grammar1 = nltk.CFG.fromstring("""
  Si -> NPi VPi
  VPi -> Vi NPi | Vi NPi PPi
  PPi -> Pi NPi
  Vi -> "saw" | "ate" | "walked"
  NPi -> "John" | "Mary" | "Bob" | Deti Ni | Deti Ni PPi
  Deti -> "a" | "an" | "the" | "my"
  Ni -> "man" | "dog" | "cat" | "telescope" | "park"
  Pi -> "in" | "on" | "by" | "with"
  """)

sent = "Mary saw Bob".split()
rd_parser = nltk.RecursiveDescentParser(grammar1)
for tree in rd_parser.parse(sent):
    print(tree)
