__author__ = 'Shweta'
import nltk
from nltk.corpus import PlaintextCorpusReader
import codecs
#from nltk.probability import FreqDist as FD
from nltk.probability import ConditionalFreqDist as CFD
from nltk.probability import ConditionalProbDist as CPD
from nltk.probability import LidstoneProbDist # Additive smoothing, new name...
import re

with open('tweets.txt', 'r') as tweetFile:
    with codecs.open('decTweets.txt', 'w', encoding='ascii', errors='ignore') as dtweets:
        for line in tweetFile:
            line = line.decode('ISO-8859-1', 'ignore').encode('ascii', 'ignore')
            dtweets.write(line+'\n')

corpus = PlaintextCorpusReader('./', ".*decTweets\.txt")
text = nltk.Text(corpus.words())


len(text)
bigrams = nltk.ngrams(text, 2) # shortcut: nltk.bigrams(text)
cfd = CFD(bigrams)

languageModel = CPD(cfd, LidstoneProbDist, 2.)
i = 0

word = 'http'
while i != 10:
    sentence = list()
    while len(' '.join(sentence)) < 100:
        #print(word)
        sentence.append(word)
        word = languageModel[word].generate()
    finalSent = ' '.join(sentence)
    finalSent = re.sub(' # ', ' #', finalSent)
    finalSent = re.sub('# ', '#', finalSent)
    finalSent = re.sub(r' http :// t ?\. ', ' http://t.', finalSent)
    finalSent = re.sub(r' ?@ ', ' @', finalSent)
    finalSent = re.sub(r' https :// t ?\. ', ' https://t.', finalSent)
    finalSent = re.sub(' . co', '', finalSent)
    print finalSent
    print
    print
    i += 1
