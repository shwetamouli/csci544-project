import sys
import os
sys.path.append('ark-tweet-nlp-0.3.2')
import CMUTweetTagger
import random
import pickle

def triples(wlist):
 if(len(wlist)<3):return
 for i in range(len(wlist)-2):
  yield(wlist[i],wlist[i+1],wlist[i+2])


def ispos(pos):
 def iswpos(word):
  return word_pos_dict[word]==pos
 return iswpos 

def tweetify(text):
 print text
 return text[0].upper()+text[1:]+'.'
 

#seedpair=pickle.load(open('../goodtweets/seedpair.txt','rb'))
#print(seedpair)

def gentweet():
#now generate stuff purely based on word assumptions
 seed=random.randint(0,len(words)-3)
 finaltweet=[]
 w1=words[seed]
 w2=words[seed+1]
 for i in range(25):
  finaltweet.append(w1)
  w1,w2=w2,random.choice(markov_dict[(w1,w2)])
 finaltweet.append(w2)
 return finaltweet

def gentweetpos():
 seed=random.randint(0,len(words)-3)
 finaltweet=[]
 w1=words[seed]
 w2=words[seed+1]
 finaltweet.append(w1)
 for i in range(25):
  t=(w1,w2)
  w1=w2
  finaltweet.append(w1)
  pos2=random.choice(word_pos_dict[w2])
  ispos_static=ispos(pos2)
  word_shortlist=filter(ispos_static,markov_dict[t])
  if not word_shortlist: 
   w2=random.choice(markov_dict[t])
  else:
   w2=random.choice(word_shortlist)
 finaltweet.append(w2) 
 return finaltweet







data=sys.argv[1] #file containing tweets
f=open(data,'r')
tweets=f.readlines()
words=[]
tweet_pos_list=CMUTweetTagger.runtagger_parse(tweets)
word_pos_dict={}

#append eos
for tweet in tweet_pos_list:
 tweet.append(('EOS','EOS',1))
 
#build word:pos dict now
for tweet in tweet_pos_list:
 for word in tweet:
  word_pos_dict[word[0]]=word[1]
  words.append(word[0])
#print tweet_pos_list

#build dictionary of w1,w2:w3

markov_dict={}
for w1,w2,w3 in triples(words):
 if (w1,w2) not  in markov_dict:
  markov_dict[(w1,w2)]=[w3]
 else:
  markov_dict[(w1,w2)].append(w3)
#for item in markov_dict:
# print str(item)+':'+str(markov_dict[item])
#print words

#build dictionary of pos1,pos2:pos3
#first store pos from file, preserving order
poslist=[word_pos_dict[word] for word in words ]
markov_pos_dict={}
for pos1,pos2,pos3 in triples(poslist):
 if (pos1,pos2) not  in markov_pos_dict:
  markov_pos_dict[(pos1,pos2)]=[pos3]
 else:
  markov_pos_dict[(pos1,pos2)].append(pos3)



#print(markov_pos_dict)
#now generate stuff purely based on word assumptions
'''seed=random.randint(0,len(words)-3)
finaltweet=[]
w1=words[seed]
w2=words[seed+1]
for i in range(25):
 finaltweet.append(w1)
 w1,w2=w2,random.choice(markov_dict[(w1,w2)])
finaltweet.append(w2)'''

finaltweet=gentweet()
#print only till eos
for i in range(len(finaltweet)):
 if finaltweet[i]=='EOS':
  break
  
finaltweet=finaltweet[:i]
print(' '.join(finaltweet))
print
print

#now generate stuff with postags in mind
'''seed=random.randint(0,len(words)-3)
finaltweet=[]
w1=words[seed]
w2=words[seed+1]
finaltweet.append(w1)
for i in range(25):
 t=(w1,w2)
 w1=w2
 finaltweet.append(w1)
 pos2=random.choice(word_pos_dict[w2])
 ispos_static=ispos(pos2)
 word_shortlist=filter(ispos_static,markov_dict[t])
 if not word_shortlist: 
  w2=random.choice(markov_dict[t])
 else:
  w2=random.choice(word_shortlist)
finaltweet.append(w2)'''

finaltweet=gentweetpos()
#print only till eos
for i in range(len(finaltweet)):
 if finaltweet[i]=='EOS':
  break
  
finaltweet=finaltweet[:i]
print ' '.join(finaltweet)
print
print


 


